<?php
/*
  Policies look like:
  <site-control permitted-cross-domain-policies="none"/>
*/
?>
<!DOCTYPE cross-domain-policy SYSTEM "http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">
<cross-domain-policy>
	<?php print $processed_policies ?>
</cross-domain-policy>